import React from 'react';
import {View, TouchableOpacity} from 'react-native';

import {BoldText, LightText} from '../Components/Common/StyledTexts';

import Color from '../Styles/Color';

function ErrorHandler(props) {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
      <View style={{paddingHorizontal: 20}}>
        <BoldText style={{fontSize: 70, color: Color.gray04}}>
          Something Went Wrong at our End
        </BoldText>
        <View style={{marginVertical: 30}} />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate('Home', {tryAgainStatus: true})
            }
            style={{
              borderWidth: 1,
              borderColor: Color.gray04,
              paddingHorizontal: 30,
              paddingVertical: 10,
            }}>
            <LightText style={{fontSize: 25, letterSpacing: 2}}>
              Retry
            </LightText>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

export default ErrorHandler;
