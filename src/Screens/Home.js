import React, {useEffect, useState} from 'react';
import {View, PermissionsAndroid} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import moment from 'moment';

import {BoldText} from '../Components/Common/StyledTexts';
import Loader from '../Components/Common/Loader';
import {api, googleApi} from '../API';
import {CONSTANTS} from '../Constants/Constants';
import WeatherForecast from '../Components/WeatherForecast';
import {addForecastData, setLoading} from '../Actions/ActionCreator';
import store from '../Store';

function Home(props) {
  const [coordintates, setCoordinates] = useState({lat: 0, long: 0});
  const [currentCity, setCurrentCity] = useState('');
  const [currentWeatherData, setCurrentWeatherData] = useState({
    day: 'Sunday',
    temp: 273,
  });

  const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Weather App Location Permission',
          message: 'Weather App needs access to your location ',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        store.dispatch(setLoading(true));
        console.log('Location permission Granted');
      } else {
        console.log('Please grant turn on your location');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    findGPSLocation();
  }, [0]);

  useEffect(() => {
    fetchCurrentCity();
    getWeatherData();
  }, [coordintates]);

  useEffect(() => {
    if (
      props.navigation.state &&
      props.navigation.state.params &&
      props.navigation.state.params.tryAgainStatus
    ) {
      getWeatherData();
    }
  }, [props.navigation]);

  async function fetchCurrentCity() {
    try {
      let googlePlaceResponse = await googleApi.get('/geocode/json', {
        latlng: `${coordintates.lat},${coordintates.long}`,
        sensor: true,
        key: CONSTANTS.APP_CONFIG.MAP_APP_KEY,
      });
      if (
        googlePlaceResponse.data.results &&
        googlePlaceResponse.data.results.length > 0
      ) {
        let {address_components} = googlePlaceResponse.data.results[0];
        address_components.forEach((item) => {
          if (item.types.includes('administrative_area_level_2')) {
            setCurrentCity(item.long_name);
          }
        });
      }
    } catch (error) {
      store.dispatch(setLoading(false));
      console.log(' error ', error);
    }
  }

  async function findGPSLocation() {
    await requestLocationPermission();
    Geolocation.getCurrentPosition(
      (position) => {
        setCoordinates({
          lat: position.coords.latitude,
          long: position.coords.longitude,
        });
      },
      (error) => {
        console.log(' error ', error);
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
    );
  }

  async function getWeatherData() {
    if (coordintates.lat && coordintates.long) {
      setTimeout(async () => {
        try {
          let weatherResponse = await api.get('/onecall', {
            lat: coordintates.lat,
            lon: coordintates.long,
            appid: CONSTANTS.APP_CONFIG.WEATHER_APP_ID,
            exclude: 'minutely,hourly',
          });
          if (weatherResponse.ok) {
            return storeWeatherData(weatherResponse.data);
          }
          store.dispatch(setLoading(false));
          props.navigation.navigate('ErrorHandler');
        } catch (error) {
          props.navigation.navigate('ErrorHandler');
        }
      });
    }
  }

  function storeWeatherData(data) {
    let currentDate = moment.unix(data.current.dt),
      temp = data.current.temp.toFixed(0);
    setCurrentWeatherData({
      day: CONSTANTS.WEEKDAYS[currentDate.isoWeekday()],
      temp: temp,
    });
    if (data.daily) {
      let filterWeatherData = data.daily.map((data) => {
        let date = moment.unix(data.dt);
        return {
          day: CONSTANTS.WEEKDAYS[date.isoWeekday()],
          temp: (data.temp.day - 273).toFixed(0),
        };
      });
      store.dispatch(addForecastData(filterWeatherData));
    }
    store.dispatch(setLoading(false));
  }

  return (
    <View style={{flex: 1}}>
      <View style={{flex: 0.5}}>
        <View style={{flex: 0.15}} />
        <View
          style={{
            flex: 0.85,
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <BoldText style={{fontSize: 100, alignSelf: 'center'}}>
            {currentWeatherData.temp - 273}
          </BoldText>
          <BoldText
            style={{
              fontSize: 40,
              alignSelf: 'center',
            }}>
            {currentCity}
          </BoldText>
        </View>
      </View>
      <View style={{flex: 0.5}}>
        <WeatherForecast />
      </View>
      <Loader />
    </View>
  );
}

export default Home;
