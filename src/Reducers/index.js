import {ACTIONS} from '../Actions/Action';

const initialState = {
  forecastData: [],
  loading: false,
};

export function rootReducer(state = initialState, action) {
  if (action.type === ACTIONS.ADD_FORECAST_DATA) {
    state = {...state, forecastData: action.payload};
  }
  if (action.type === ACTIONS.LOADING) {
    state = {...state, loading: action.payload};
  }
  return state;
}
