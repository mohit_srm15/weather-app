import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Splash from './Screens/Splash';
import Home from './Screens/Home';
import ErrorHandler from "./Screens/Error";

const appNavigations = createStackNavigator(
  {
    Splash: {screen: Splash},
    Home: {screen: Home},
    ErrorHandler: {screen: ErrorHandler},
  },
  {
    headerMode: 'none',
  },
);

export const AppContainer = createAppContainer(appNavigations);
