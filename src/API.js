import {create } from 'apisauce';

export const api = create({
  baseURL: 'https://api.openweathermap.org/data/2.5',
  headers: {Accept: 'application/vnd.github.v3+json'},
  timeout: 3000
});

export const googleApi = create({
  baseURL: 'https://maps.googleapis.com/maps/api/',
  headers: {Accept: 'application/vnd.github.v3+json'},
  timeout: 3000,
});
