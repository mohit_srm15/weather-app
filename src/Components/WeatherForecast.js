import React from 'react';
import {FlatList, View} from 'react-native';
import {connect} from 'react-redux';
import Color from '../Styles/Color';
import {LightText} from './Common/StyledTexts';

function WeatherForecast(props) {
  let {forecastData} = props.state;
  return (
    <View>
      <FlatList
        data={
          forecastData.length > 0
            ? forecastData.filter((item, i) => i > 0 && i <= 5)
            : []
        }
        style={{borderTopWidth: 1, borderColor: Color.gray04}}
        keyExtractor={(item, index) => index.toString()}
        ItemSeparatorComponent={() => {
          return (
            <View
              style={{flex: 1, backgroundColor: Color.gray04, height: 0.6}}
            />
          );
        }}
        renderItem={({item}) => {
          return (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 30,
                paddingVertical: 15,
              }}>
              <LightText style={{fontSize: 30}}>{item.day}</LightText>
              <LightText style={{fontSize: 30}}>{item.temp}</LightText>
            </View>
          );
        }}
      />
    </View>
  );
}
const mapStateToProps = (state) => {
  return {state};
};

export default WeatherForecast = connect(mapStateToProps)(WeatherForecast);
