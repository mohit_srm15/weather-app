import {ACTIONS} from './Action';

export function addForecastData(payload) {
  return {
    type: ACTIONS.ADD_FORECAST_DATA,
    payload: payload,
  };
}

export function setLoading(payload) {
  return {
    type: ACTIONS.LOADING,
    payload: payload,
  };
}
